package main

import "fmt"

func cetak(ch chan int) {
	fmt.Println("ini dari gorotuine...")
	ch <- 10
}

func main() {
	chdata := make(chan int)
	go cetak(chdata)
	nilai := <-chdata
	fmt.Println("nilai channel integer :", nilai)
	fmt.Println("ini dari fungsi main...")
}
