package main

import (
	"fmt"
	"log"
	"net/http"
)

const port string = "9090"

func welcome(w http.ResponseWriter, r *http.Request) {
	dataHTML := `<h1>Kumpulan Bahasa Pemrograman Keren</h1>
		<ul>
			<li>Golang</li>
			<li>PHP</li>
			<li>JavaScript</li>
			<li>Rust</li>
			<li>Java</li>
		</ul>
		Tutorial by : <a href="https://kodingin.com">Kodingin</a>
		`
	if r.Method == "GET" {
		fmt.Fprintf(w, dataHTML)
	}
}

func main() {
	fmt.Println("Berjalan di Port :", port)
	http.HandleFunc("/", welcome)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		log.Fatal(err)
	}
}
