// Ketika kita memberikan nilai pada interface berupa variable objek ,
// tentunya akan menimplementasi sebuah method yang di dalam interface.
// Di dalam golang interface tersebut dapat di jadikan parameter terhadap fungsi lain,
// walaupun fungsi tersebut tidak ada di dalam interface.

package main

import (
	"fmt"
)

type Kendaraan interface {
	GetWarna()
}

type Mobil struct {
	Depan    string
	Belakang string
	Atas     string
	Dalam    string
}

func (m Mobil) GetWarna() {
	fmt.Println(m)
}

func CetakWarna(w Kendaraan) {
	fmt.Println(w)
}

func main() {
	var k Kendaraan
	mobil := Mobil{
		Depan:    "Biru",
		Belakang: "Biru",
		Atas:     "Pink",
		Dalam:    "Putih",
	}

	k = mobil
	CetakWarna(k)
}

// Perhatikan kode golang di atas, terdapat interface dengan nama kendaraan, yang memiliki method GetWarna().
// Selain itu juga terdapat fungsi yang tidak ada di dalam interface.
// Maka untuk menggunakan nilai interface dapat di jadikan parameter terhadap fungsi lain.
// Contoh di atas yang menggunakan parameter nilai interface yaitu CetakWarna().
// Di dimana di dalam fungsi tersebut berisi nilai yang di masukan variable objek interface.
