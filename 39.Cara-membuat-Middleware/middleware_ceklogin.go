package main

import (
	"fmt"
	"net/http"
)

// Fungi CekLogin yang berguna sebagai middleware
func CekLogin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.URL.Query().Get("token") != "12345" {
			fmt.Fprintf(w, "Token tidak tersedia atau salah\n")
			return
		}
		next.ServeHTTP(w, r)
	})
}

// Fungsi GetMahasiswa untuk mengampilkan text string di browser
func GetMahasiswa(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("<h1>Anda Berhasil Mengakses Fungsi GetMahasiswa() </h1>"))
}

func main() {

	// konfigurasi server
	server := &http.Server{
		Addr: ":8000",
	}
	// routing
	http.Handle("/", CekLogin(http.HandlerFunc(GetMahasiswa)))

	// jalankan server
	server.ListenAndServe()
}
