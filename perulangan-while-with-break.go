package main

import (
	"fmt"
)

func main() {

	var index = 0

	for index < 4 {

		fmt.Println("Angka ke", index)
		index++

		if index == 2 {
			break
		}
	}
}
