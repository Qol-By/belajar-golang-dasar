package main

import "fmt"

func main() {
	var a chan int
	if a == nil {
		a = make(chan int)
		fmt.Printf("Channel ini mempunya tipe :  %T\n", a)
	}
}
